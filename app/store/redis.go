package store

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

const PeerConfKey = "peer__conf"

func redisPeerConfKey(pubkey, ip string) string {
	return fmt.Sprintf("%s__%s__%s", PeerConfKey, pubkey, ip)
}

type Store interface {
	SetPeerConf(ctx context.Context, conf wgtypes.PeerConfig, pubkey, ip string, ttl time.Duration) error
	PeerConfs(ctx context.Context) ([]wgtypes.PeerConfig, error)
	Subscribe(ctx context.Context, channels ...string) *redis.PubSub
}

type store struct {
	rc     *redis.Client
	logger echo.Logger
}

func New(client *redis.Client, logger echo.Logger) (Store, error) {
	return &store{
		client,
		logger,
	}, nil
}

func (s *store) SetPeerConf(ctx context.Context, conf wgtypes.PeerConfig, pubkey, ip string, ttl time.Duration) error {
	confJSON, err := json.Marshal(conf)
	if err != nil {
		s.logger.Errorf("could not marshal peer config to json %v", err)
		return err
	}

	key := redisPeerConfKey(pubkey, ip)
	err = s.rc.Set(ctx, key, confJSON, ttl).Err()
	if err != nil {
		s.logger.Errorf("could not set peer config to redis %v", err)
		return err
	}

	return nil
}

func (s *store) PeerConfs(ctx context.Context) ([]wgtypes.PeerConfig, error) {
	keys, err := s.rc.Keys(ctx, PeerConfKey+"*").Result()
	if err != nil {
		s.logger.Errorf("could not get peers by key %s %v", PeerConfKey+"*", err)
		return nil, err
	}

	if len(keys) == 0 {
		return nil, nil
	}

	var confs []wgtypes.PeerConfig
	for _, key := range keys {
		val, err := s.rc.Get(ctx, key).Result()
		if err != nil {
			s.logger.Error("could not get peer config from key %v %v", key, err)
			return nil, err
		}

		var conf wgtypes.PeerConfig
		if err := json.Unmarshal([]byte(val), &conf); err != nil {
			s.logger.Error("could not parse peer config %v", key, err)
			return nil, err
		}

		confs = append(confs, conf)
	}

	return confs, nil
}

func (s *store) Subscribe(ctx context.Context, channels ...string) *redis.PubSub {
	return s.rc.Subscribe(ctx, channels...)
}
