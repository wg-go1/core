package ipcontroller

import (
	"context"
	"errors"
	"math/rand"
	"net"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
)

var ErrNoAvailableIps = errors.New("no available ips")

var hashName = "map:ips"

type IPController interface {
	State(ctx context.Context) (map[string]bool, error)
	Take(ctx context.Context) (string, error)
	GetAway(ctx context.Context, ip string) error
	Restore(ctx context.Context, ips []string) error
}

type s struct {
	redisClient *redis.Client
	logger      echo.Logger
}

func New(ctx context.Context, wg_cidr string, wg_gateway net.IP, redisClient *redis.Client, logger echo.Logger) (IPController, error) {
	ips := prepareIps(wg_cidr, wg_gateway, logger)

	for _, ip := range ips {
		// ip - is available
		err := redisClient.HSet(ctx, hashName, ip.String(), true).Err()
		if err != nil {
			logger.Errorf("could not set ip %s: %v", ip.String(), err)
			return nil, err
		}
	}

	return &s{
		redisClient: redisClient,
		logger:      logger,
	}, nil
}

func (s *s) State(ctx context.Context) (map[string]bool, error) {
	data, err := s.redisClient.HGetAll(ctx, hashName).Result()
	if err != nil {
		return nil, err
	}

	result := make(map[string]bool)
	for k, v := range data {
		if v == "1" {
			result[k] = true
		} else {
			result[k] = false
		}
	}

	return result, nil
}

func (s *s) Take(ctx context.Context) (string, error) {
	rand.Seed(time.Now().UnixNano())

	ips, err := s.State(ctx)
	if err != nil {
		s.logger.Errorf("could not get state: %v", err)
		return "", err
	}

	var availableIps []string
	for ip, available := range ips {
		if available {
			availableIps = append(availableIps, ip)
		}
	}

	if len(availableIps) == 0 {
		return "", ErrNoAvailableIps
	}

	randomIndex := rand.Intn(len(availableIps))
	randomIp := availableIps[randomIndex]

	err = s.redisClient.HSet(ctx, hashName, randomIp, false).Err()
	if err != nil {
		s.logger.Errorf("could not book ip %s: %v", randomIp, err)
		return "", err
	}

	return randomIp, nil
}

func (s *s) GetAway(ctx context.Context, ip string) error {
	err := s.redisClient.HSet(ctx, hashName, ip, true).Err()
	if err != nil {
		s.logger.Errorf("could not get away ip %s: %v", ip, err)
		return err
	}
	return nil
}

func (s *s) Restore(ctx context.Context, ips []string) error {
	for _, ip := range ips {
		err := s.redisClient.HSet(ctx, hashName, ip, false).Err()
		if err != nil {
			s.logger.Errorf("could not set ip %s: %v", ip, err)
			return err
		}
	}
	return nil
}

func prepareIps(wg_cidr string, wg_gateway net.IP, logger echo.Logger) []net.IP {
	ip, ipNet, err := net.ParseCIDR(wg_cidr)
	if err != nil {
		logger.Errorf("could not parse CIDR from %s: %v", wg_cidr, err)
		return nil
	}

	var allIPs []net.IP
	for ip := ip.Mask(ipNet.Mask).To4(); ipNet.Contains(ip); inc(ip) {
		if wg_gateway.Equal(ip) || ipNet.IP.Equal(ip) {
			continue
		}
		allIPs = append(allIPs, net.IPv4(ip[0], ip[1], ip[2], ip[3]))
	}

	return allIPs
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
