package ipcontroller

import (
	"context"
	"net"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/require"
)

func prepare(t *testing.T) (*miniredis.Miniredis, IPController) {
	e := echo.New()
	mr, err := miniredis.Run()
	require.NoError(t, err)

	client := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})

	ipcontroller, err := New(context.Background(), "10.8.0.0/29", net.ParseIP("10.8.0.1"), client, e.Logger)
	require.NoError(t, err)

	return mr, ipcontroller
}

func TestInitialState(t *testing.T) {
	mr, ipcontroller := prepare(t)
	defer mr.Close()

	state, err := ipcontroller.State(context.Background())
	require.NoError(t, err)

	expected := map[string]bool{
		"10.8.0.2": true,
		"10.8.0.3": true,
		"10.8.0.4": true,
		"10.8.0.5": true,
		"10.8.0.6": true,
		"10.8.0.7": true,
	}

	require.Equal(t, expected, state)
}

func TestTake(t *testing.T) {
	mr, ipcontroller := prepare(t)
	defer mr.Close()

	ip, err := ipcontroller.Take(context.Background())
	require.NoError(t, err)

	state, err := ipcontroller.State(context.Background())
	require.NoError(t, err)

	for key, val := range state {
		require.Equal(t, key != ip, val)
	}
}

func TestErrNoAvailableIps(t *testing.T) {
	mr, ipcontroller := prepare(t)
	defer mr.Close()

	state, err := ipcontroller.State(context.Background())
	require.NoError(t, err)

	for i := 0; i < len(state); i++ {
		_, err := ipcontroller.Take(context.Background())
		require.NoError(t, err)
	}

	_, err = ipcontroller.Take(context.Background())
	require.Equal(t, ErrNoAvailableIps, err)

}

func TestGetAway(t *testing.T) {
	mr, ipcontroller := prepare(t)
	defer mr.Close()

	ip, err := ipcontroller.Take(context.Background())
	require.NoError(t, err)

	err = ipcontroller.GetAway(context.Background(), ip)
	require.NoError(t, err)

	state, err := ipcontroller.State(context.Background())
	require.NoError(t, err)

	for _, val := range state {
		require.Equal(t, true, val)
	}
}

func TestRestore(t *testing.T) {
	mr, ipcontroller := prepare(t)
	defer mr.Close()

	err := ipcontroller.Restore(context.Background(), []string{
		"10.8.0.2", "10.8.0.3", "10.8.0.4",
	})
	require.NoError(t, err)

	state, err := ipcontroller.State(context.Background())
	require.NoError(t, err)

	expected := map[string]bool{
		"10.8.0.2": false,
		"10.8.0.3": false,
		"10.8.0.4": false,
		"10.8.0.5": true,
		"10.8.0.6": true,
		"10.8.0.7": true,
	}

	require.Equal(t, expected, state)
}
