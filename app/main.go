package main

import (
	"context"
	"io/ioutil"
	"net"
	"os/exec"
	"strconv"

	"server/api"
	"server/ipcontroller"
	"server/manager"
	"server/store"
	"server/wg"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func prepareWg0(wg_gateway net.IP, wg_net *net.IPNet, wg_port int, logger echo.Logger) error {
	mask, _ := wg_net.Mask.Size()
	maskStr := strconv.Itoa(mask)
	portStr := strconv.Itoa(wg_port)

	skWg0, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		logger.Errorf("could not generate private key %v", err)
		return err
	}

	_wg0Conf := []byte(`[Interface]
PrivateKey = ` + skWg0.String() + `
Address = ` + wg_gateway.String() + `/` + maskStr + `
ListenPort = ` + portStr + `
PostUp =  iptables -t nat -A POSTROUTING -s 0.0.0.0/0 -o any -j MASQUERADE; iptables -A INPUT -p udp -m udp --dport ` + portStr + ` -j ACCEPT; iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT;
	`)

	err = ioutil.WriteFile("/etc/wireguard/wg0.conf", _wg0Conf, 0644)
	if err != nil {
		logger.Errorf("could not write config to /etc/wireguard/wg0.conf %v", err)
		return err
	}

	logger.Info("wg0.conf file created successfully")

	out, err := exec.Command("wg-quick", "up", "wg0").CombinedOutput()
	if err != nil {
		logger.Errorf("could not execute wg-quick up wg0: %v", err)
		return err
	}

	logger.Info("wg-quick up wg0 successfully \n\n", string(out))

	return nil
}

func main() {
	e := echo.New()
	e.Logger.SetLevel(log.DEBUG)
	ctx := context.Background()
	cfg := loadConfig(e.Logger)

	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.appRedis,
		Password: cfg.appRedisPass,
		DB:       0,
	})
	defer redisClient.Close()

	_, err := redisClient.Ping(ctx).Result()
	if err != nil {
		e.Logger.Fatal(err)
	}

	_ipcontroller, err := ipcontroller.New(ctx, cfg.wgCidr, cfg.wgGateway, redisClient, e.Logger)
	if err != nil {
		e.Logger.Fatal(err)
	}

	err = prepareWg0(cfg.wgGateway, cfg.wgNet, cfg.wgPort, e.Logger)
	if err != nil {
		e.Logger.Fatal(err)
	}

	_store, err := store.New(redisClient, e.Logger)
	if err != nil {
		e.Logger.Fatal(err)
	}

	wgc, err := wgctrl.New()
	if err != nil {
		e.Logger.Fatal(err)
	}

	defer wgc.Close()

	_wg, err := wg.New(wgc, e.Logger, cfg.wgPort)
	if err != nil {
		e.Logger.Fatal(err)
	}

	_manager, err := manager.New(ctx, _store, _ipcontroller, _wg, e.Logger, cfg.whoAmI)
	if err != nil {
		e.Logger.Fatal(err)
	}

	expiredKey := _store.Subscribe(ctx, "__keyevent@0__:expired")
	defer func(expiredKey *redis.PubSub) {
		err := expiredKey.Close()
		if err != nil {
			e.Logger.Fatal(err)
		}
	}(expiredKey)

	go func() {
		for msg := range expiredKey.Channel() {
			err := _manager.HandleExpiredKey(msg.Payload)
			if err != nil {
				e.Logger.Fatal(err)
			}
		}
	}()

	e.Logger.Info("initial successfully")

	_api := api.New(_manager)

	e.Use(middleware.KeyAuthWithConfig(middleware.KeyAuthConfig{
		KeyLookup: "header:X-API-Key",
		Validator: func(key string, c echo.Context) (bool, error) {
			return key == cfg.appAuthKey, nil
		},
	}))
	e.GET("/newPeer", _api.NewPeerHandler)
	e.Logger.Fatal(e.Start(":" + cfg.appHttpPort))
}
