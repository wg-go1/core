package main

import (
	"net"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

type config struct {
	wgCidr    string
	wgGateway net.IP
	wgNet     *net.IPNet
	wgPort    int

	appRedis     string
	appRedisPass string
	appHttpPort  string
	appAuthKey   string

	whoAmI string
}

func loadConfig(logger echo.Logger) *config {
	err := godotenv.Load("../config/.env")
	if err != nil {
		logger.Fatalf("error loading config: %v", err)
	}

	wgCidr := os.Getenv("WG_CIDR")
	if wgCidr == "" {
		logger.Fatalf("missing WG_CIDR environment variable")
	}

	_, ipNet, err := net.ParseCIDR(wgCidr)
	if err != nil {
		logger.Fatalf("could not parse WG_CIDR: %v", err)
	}

	gatewayIP := net.IP(make([]byte, 4))
	copy(gatewayIP, ipNet.IP.To4())
	gatewayIP[3]++

	wgPort := os.Getenv("WG_PORT")
	if wgPort == "" {
		logger.Fatalf("missing WG_PORT environment variable")
	}

	wgPortInt, err := strconv.Atoi(wgPort)
	if err != nil {
		logger.Fatalf("could not convert wg_port %v to int", wgPort)
	}

	appRedis := os.Getenv("APP_REDIS")
	if appRedis == "" {
		logger.Fatalf("missing APP_REDIS environment variable")
	}

	appRedisPass := os.Getenv("APP_REDIS_PASS")

	appHttpPort := os.Getenv("APP_HTTP_PORT")
	if appHttpPort == "" {
		logger.Fatalf("missing APP_HTTP_PORT environment variable")
	}

	appAuthKey := os.Getenv("APP_AUTH_KEY")

	whoAmI := os.Getenv("WHO_AM_I")
	if wgPort == "" {
		logger.Fatalf("missing WHO_AM_I environment variable")
	}

	return &config{
		wgCidr:    wgCidr,
		wgGateway: gatewayIP,
		wgNet:     ipNet,
		wgPort:    wgPortInt,

		appRedis:     appRedis,
		appRedisPass: appRedisPass,
		appHttpPort:  appHttpPort,
		appAuthKey:   appAuthKey,

		whoAmI: whoAmI,
	}
}
