package api

import (
	"context"
	"net/http"
	"server/manager"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
)

type Api interface {
	NewPeerHandler(c echo.Context) error
}

type api struct {
	manager manager.Manager
}

func New(manager manager.Manager) Api {
	return &api{manager}
}

type GenPeerHandlerResponse struct {
	Config string    `json:"config"`
	DateTo time.Time `json:"date_to"`
}

func (a *api) NewPeerHandler(c echo.Context) error {
	mins, err := strconv.Atoi(c.QueryParam("duration_in_minutes"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	duration := time.Minute * time.Duration(mins)

	ctx := context.Background()

	out, err := a.manager.NewPeer(ctx, duration)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, GenPeerHandlerResponse{
		Config: string(out),
		DateTo: time.Now().Add(duration).UTC(),
	})
}
