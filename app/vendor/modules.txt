# github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a
## explicit
github.com/alicebob/gopher-json
# github.com/alicebob/miniredis v2.5.0+incompatible
## explicit
github.com/alicebob/miniredis
github.com/alicebob/miniredis/server
# github.com/alicebob/miniredis/v2 v2.30.1
## explicit; go 1.14
# github.com/cespare/xxhash/v2 v2.1.2
## explicit; go 1.11
github.com/cespare/xxhash/v2
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f
## explicit
github.com/dgryski/go-rendezvous
# github.com/go-redis/redis/v8 v8.11.5
## explicit; go 1.17
github.com/go-redis/redis/v8
github.com/go-redis/redis/v8/internal
github.com/go-redis/redis/v8/internal/hashtag
github.com/go-redis/redis/v8/internal/hscan
github.com/go-redis/redis/v8/internal/pool
github.com/go-redis/redis/v8/internal/proto
github.com/go-redis/redis/v8/internal/rand
github.com/go-redis/redis/v8/internal/util
# github.com/golang-jwt/jwt v3.2.2+incompatible
## explicit
github.com/golang-jwt/jwt
# github.com/gomodule/redigo v1.8.9
## explicit; go 1.16
github.com/gomodule/redigo/redis
# github.com/google/go-cmp v0.5.9
## explicit; go 1.13
github.com/google/go-cmp/cmp
github.com/google/go-cmp/cmp/internal/diff
github.com/google/go-cmp/cmp/internal/flags
github.com/google/go-cmp/cmp/internal/function
github.com/google/go-cmp/cmp/internal/value
# github.com/google/uuid v1.3.0
## explicit
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
# github.com/joho/godotenv v1.5.1
## explicit; go 1.12
github.com/joho/godotenv
# github.com/josharian/native v1.0.0
## explicit; go 1.13
github.com/josharian/native
# github.com/labstack/echo/v4 v4.10.2
## explicit; go 1.17
github.com/labstack/echo/v4
github.com/labstack/echo/v4/middleware
# github.com/labstack/gommon v0.4.0
## explicit; go 1.12
github.com/labstack/gommon/bytes
github.com/labstack/gommon/color
github.com/labstack/gommon/log
github.com/labstack/gommon/random
# github.com/mattn/go-colorable v0.1.13
## explicit; go 1.15
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.17
## explicit; go 1.15
github.com/mattn/go-isatty
# github.com/mdlayher/genetlink v1.2.0
## explicit; go 1.13
github.com/mdlayher/genetlink
# github.com/mdlayher/netlink v1.6.2
## explicit; go 1.13
github.com/mdlayher/netlink
github.com/mdlayher/netlink/nlenc
# github.com/mdlayher/socket v0.2.3
## explicit; go 1.17
github.com/mdlayher/socket
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/stretchr/objx v0.5.0
## explicit; go 1.12
github.com/stretchr/objx
# github.com/stretchr/testify v1.8.2
## explicit; go 1.13
github.com/stretchr/testify/assert
github.com/stretchr/testify/mock
github.com/stretchr/testify/require
# github.com/valyala/bytebufferpool v1.0.0
## explicit
github.com/valyala/bytebufferpool
# github.com/valyala/fasttemplate v1.2.2
## explicit; go 1.12
github.com/valyala/fasttemplate
# github.com/yuin/gopher-lua v1.1.0
## explicit; go 1.17
github.com/yuin/gopher-lua
github.com/yuin/gopher-lua/ast
github.com/yuin/gopher-lua/parse
github.com/yuin/gopher-lua/pm
# golang.org/x/crypto v0.6.0
## explicit; go 1.17
golang.org/x/crypto/acme
golang.org/x/crypto/acme/autocert
golang.org/x/crypto/curve25519
golang.org/x/crypto/curve25519/internal/field
# golang.org/x/net v0.7.0
## explicit; go 1.17
golang.org/x/net/bpf
golang.org/x/net/http/httpguts
golang.org/x/net/http2
golang.org/x/net/http2/h2c
golang.org/x/net/http2/hpack
golang.org/x/net/idna
# golang.org/x/sync v0.1.0
## explicit
golang.org/x/sync/errgroup
# golang.org/x/sys v0.5.0
## explicit; go 1.17
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.7.0
## explicit; go 1.17
golang.org/x/text/secure/bidirule
golang.org/x/text/transform
golang.org/x/text/unicode/bidi
golang.org/x/text/unicode/norm
# golang.org/x/time v0.3.0
## explicit
golang.org/x/time/rate
# golang.zx2c4.com/wireguard v0.0.0-20220920152132-bb719d3a6e2c
## explicit; go 1.19
golang.zx2c4.com/wireguard/ipc/namedpipe
# golang.zx2c4.com/wireguard/wgctrl v0.0.0-20230215201556-9c5414ab4bde
## explicit; go 1.19
golang.zx2c4.com/wireguard/wgctrl
golang.zx2c4.com/wireguard/wgctrl/internal/wgfreebsd
golang.zx2c4.com/wireguard/wgctrl/internal/wgfreebsd/internal/nv
golang.zx2c4.com/wireguard/wgctrl/internal/wgfreebsd/internal/wgh
golang.zx2c4.com/wireguard/wgctrl/internal/wginternal
golang.zx2c4.com/wireguard/wgctrl/internal/wglinux
golang.zx2c4.com/wireguard/wgctrl/internal/wgopenbsd
golang.zx2c4.com/wireguard/wgctrl/internal/wgopenbsd/internal/wgh
golang.zx2c4.com/wireguard/wgctrl/internal/wguser
golang.zx2c4.com/wireguard/wgctrl/internal/wgwindows
golang.zx2c4.com/wireguard/wgctrl/internal/wgwindows/internal/ioctl
golang.zx2c4.com/wireguard/wgctrl/wgtypes
# gopkg.in/yaml.v3 v3.0.1
## explicit
gopkg.in/yaml.v3
