package manager

import (
	"context"
	"net"
	"strings"
	"time"

	"server/ipcontroller"
	"server/store"
	"server/wg"

	"github.com/labstack/echo/v4"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Manager interface {
	NewPeer(ctx context.Context, ttl time.Duration) (peerConfOut, error)
	HandleExpiredKey(key string) error
}

type manager struct {
	ctx          context.Context
	store        store.Store
	ipcontroller ipcontroller.IPController
	logger       echo.Logger
	wg           wg.WgConfigurator
	whoAmI       string
}

func New(
	ctx context.Context,
	store store.Store,
	ipcontroller ipcontroller.IPController,
	wg wg.WgConfigurator,
	logger echo.Logger,
	whoAmI string,
) (Manager, error) {
	m := &manager{
		ctx:          ctx,
		ipcontroller: ipcontroller,
		store:        store,
		wg:           wg,
		logger:       logger,
		whoAmI:       whoAmI,
	}

	err := m.restorePeers(ctx)
	if err != nil {
		logger.Errorf("could not restore peers: %v", err)
		return nil, err
	}

	return m, nil
}

func (m *manager) NewPeer(ctx context.Context, ttl time.Duration) (peerConfOut, error) {
	ip, err := m.ipcontroller.Take(ctx)
	if err != nil {
		m.logger.Errorf("could not take ip: %v", err)
		return "", err
	}

	sk, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		m.logger.Errorf("could not generate private key: %v", err)
		return "", err
	}

	pubkey := sk.PublicKey()

	psk, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		m.logger.Errorf("could not generate preshared key: %v", err)
		return "", err
	}

	peerConfSys := wgtypes.PeerConfig{
		PublicKey:         sk.PublicKey(),
		PresharedKey:      &psk,
		ReplaceAllowedIPs: true,
		AllowedIPs: []net.IPNet{
			{
				IP:   net.ParseIP(ip),
				Mask: net.ParseIP(ip).DefaultMask(),
			},
		},
	}

	err = m.wg.NewPeer(peerConfSys)
	if err != nil {
		m.logger.Error("could not apply changes to wg: %v", err)
		return "", err
	}

	err = m.store.SetPeerConf(ctx, peerConfSys, pubkey.String(), ip, ttl)
	if err != nil {
		m.logger.Errorf("could not set peer conf to redis: %v", err)
		return "", err
	}

	peerConfOut := buildPeerConfOut(
		m.whoAmI,
		ip+"/29",
		sk.String(),
		psk.String(),
		m.wg.DevicePublicKey(),
		51820,
	)

	return peerConfOut, nil
}

func (m *manager) restorePeers(ctx context.Context) error {
	peerConfs, err := m.store.PeerConfs(ctx)
	if err != nil {
		m.logger.Errorf("could not get peer confs from redis: %v", err)
		return err
	}

	if len(peerConfs) == 0 {
		m.logger.Warn("no peer confs in redis")
		return nil
	}

	err = m.wg.ReplaceAllPeers(peerConfs)
	if err != nil {
		m.logger.Errorf("could not apply changes to wg: %v", err)
		return err
	}

	var ips []string
	for _, conf := range peerConfs {
		for _, v := range conf.AllowedIPs {
			ips = append(ips, v.IP.String())
		}

	}

	err = m.ipcontroller.Restore(ctx, ips)
	if err != nil {
		m.logger.Errorf("could not restore ips: %v", err)
		return err
	}

	m.logger.Infof("restored %d peer confs", len(peerConfs))
	return nil
}

func parseKey(key string) (ip string, pubkey wgtypes.Key, err error) {
	s := strings.TrimPrefix(key, store.PeerConfKey+"__")
	parts := strings.Split(s, "__")

	ip = parts[1]
	pubkey, err = wgtypes.ParseKey(parts[0])
	if err != nil {
		return "", wgtypes.Key{}, err
	}
	return ip, pubkey, nil
}

func (m *manager) HandleExpiredKey(key string) error {

	m.logger.Debugf("key %s expired ", key)

	ip, pubkey, err := parseKey(key)
	if err != nil {
		m.logger.Errorf("could not parse key: %v", err)
		return err
	}

	err = m.wg.RemovePeer(pubkey)
	if err != nil {
		m.logger.Errorf("could not remove peer: %v", err)
		return err
	}

	err = m.ipcontroller.GetAway(m.ctx, ip)
	if err != nil {
		m.logger.Errorf("could not get away ip: %v", err)
		return err
	}

	return nil
}
