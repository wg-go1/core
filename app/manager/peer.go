package manager

import "strconv"

type peerConfOut string

func buildPeerConfOut(endpoint, cidr, sk, psk, wgdPubkey string, endpointPort int) peerConfOut {
	return peerConfOut(`
[Interface]
PrivateKey = ` + sk + `
Address = ` + cidr + `
DNS = 1.1.1.1

[Peer]
PublicKey = ` + wgdPubkey + `
PresharedKey = ` + psk + `
AllowedIPs = 0.0.0.0/0, ::/0
PersistentKeepalive = 0
Endpoint = ` + endpoint + `:` + strconv.Itoa(endpointPort) + `
		`)

}
