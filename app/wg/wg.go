package wg

import (
	"github.com/labstack/echo/v4"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type WgConfigurator interface {
	NewPeer(peer wgtypes.PeerConfig) error
	ReplaceAllPeers(peers []wgtypes.PeerConfig) error
	RemovePeer(peerPubKey wgtypes.Key) error

	DevicePublicKey() string
}

const ifaceName = "wg0"

type wg struct {
	client *wgctrl.Client
	device *wgtypes.Device
	port   int
	logger echo.Logger
}

func New(client *wgctrl.Client, logger echo.Logger, port int) (WgConfigurator, error) {
	device, err := client.Device(ifaceName)
	if err != nil {
		logger.Errorf("could not get device wg0: %v", err)
		return nil, err
	}
	return wg{
		client, device, port, logger,
	}, nil
}

func (w wg) NewPeer(peer wgtypes.PeerConfig) error {
	return w.client.ConfigureDevice(ifaceName, wgtypes.Config{
		PrivateKey:   &w.device.PrivateKey,
		ListenPort:   &w.port,
		ReplacePeers: false,
		Peers:        []wgtypes.PeerConfig{peer},
	})
}

func (w wg) ReplaceAllPeers(peers []wgtypes.PeerConfig) error {
	return w.client.ConfigureDevice(ifaceName, wgtypes.Config{
		PrivateKey:   &w.device.PrivateKey,
		ListenPort:   &w.port,
		ReplacePeers: true,
		Peers:        peers,
	})
}

func (w wg) RemovePeer(peerPubKey wgtypes.Key) error {
	return w.client.ConfigureDevice(ifaceName, wgtypes.Config{
		PrivateKey: &w.device.PrivateKey,
		ListenPort: &w.port,
		Peers: []wgtypes.PeerConfig{{
			PublicKey: peerPubKey,
			Remove:    true,
		}},
	})
}

func (w wg) DevicePublicKey() string {
	return w.device.PrivateKey.PublicKey().String()
}
