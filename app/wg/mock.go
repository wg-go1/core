package wg

import (
	"github.com/stretchr/testify/mock"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type MockWgConfigurator struct {
	mock.Mock
}

func (m *MockWgConfigurator) NewPeer(peer wgtypes.PeerConfig) error {
	args := m.Called(peer)
	return args.Error(0)
}

func (m *MockWgConfigurator) ReplaceAllPeers(peers []wgtypes.PeerConfig) error {
	args := m.Called(peers)
	return args.Error(0)
}

func (m *MockWgConfigurator) RemovePeer(peerPubKey wgtypes.Key) error {
	args := m.Called(peerPubKey)
	return args.Error(0)
}

func (m *MockWgConfigurator) DevicePublicKey() string {
	args := m.Called()
	return args.String(0)
}
