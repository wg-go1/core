FROM golang:1.17.5-alpine3.15 AS builder
WORKDIR /app
COPY ./app .
RUN apk --no-cache add ca-certificates && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:3.17
RUN apk add --no-cache wireguard-tools
WORKDIR /app
COPY --from=builder /app/app /app
EXPOSE 51820/udp
EXPOSE 80
CMD ["./app"]

